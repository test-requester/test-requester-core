package com.gitlab.testrequester;

import com.gitlab.testrequester.VerifiedResponse.StatusAssertions;
import lombok.AllArgsConstructor;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TestRequesterTest {

    @Mock
    private RequestExecutor executor;
    @Mock
    private JsonMapper jsonMapper;

    @Test
    void test_factoryMethods() {
        @AllArgsConstructor
        class Params {
            RequestBuilder actual;
            RequestBuilder expected;
        }

        TestRequester requester = TestRequester.builder(executor).build();
        List<Params> allParams = Lists.of(
                new Params(requester.head("/example/head"),
                        new RequestBuilder(requester, "HEAD", "/example/head")),
                new Params(requester.options("/example/options"),
                        new RequestBuilder(requester, "OPTIONS", "/example/options")),
                new Params(requester.get("/example/get"),
                        new RequestBuilder(requester, "GET", "/example/get")),
                new Params(requester.post("/example/post"),
                        new RequestBuilder(requester, "POST", "/example/post")),
                new Params(requester.put("/example/put"),
                        new RequestBuilder(requester, "PUT", "/example/put")),
                new Params(requester.patch("/example/patch"),
                        new RequestBuilder(requester, "PATCH", "/example/patch")),
                new Params(requester.delete("/example/delete"),
                        new RequestBuilder(requester, "DELETE", "/example/delete"))
        );

        SoftAssertions.assertSoftly(softly -> {
            allParams.forEach(params -> {
                softly.assertThat(params.actual)
                        .as("%s %s", params.actual.getMethod(), params.actual.getUri())
                        .isEqualTo(params.expected);
            });
        });
    }

    @Test
    void test_getUri_noUriVariables() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example/foo/bar").build();

        assertThat(request.getUri()).isEqualTo(URI.create("/example/foo/bar"));
    }

    @Test
    void test_getUri_withUriVariable_noEncodingRequired() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example/{foo}/{foobar}")
                .withUriVariable("foo", "first")
                .withUriVariable("foobar", "second")
                .build();

        assertThat(request.getUri()).isEqualTo(URI.create("/example/first/second"));
    }

    @Test
    void test_getUri_withUriVariable_encodingRequired() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example/{foo}/{foobar}")
                .withUriVariable("foo", "first = 1")
                .withUriVariable("foobar", "second = 2")
                .build();

        assertThat(request.getUri()).isEqualTo(URI.create("/example/first+%3D+1/second+%3D+2"));
    }

    @Test
    void test_getUri_withUriVariable_encodingSkipped() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example/{first}/{second}")
                .withUriVariable("first", "foo:bar")
                .withUriVariable("second", "foo:bar", false)
                .build();

        assertThat(request.getUri()).hasToString("/example/foo%3Abar/foo:bar");
    }

    @Test
    void test_withUriVariables_canOverride() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example/{first}/{second}/{third}/{fourth}")
                .withUriVariable("first", "1")
                .withUriVariable("second", "two")
                .withUriVariable("third", "3")
                .withUriVariable("second", "2")
                .withUriVariable("fourth", "4")
                .build();

        assertThat(request.getUri()).hasToString("/example/1/2/3/4");
    }

    @Test
    void test_queryParams_canOverride() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example")
                .withQueryParams(Maps.build(params -> {
                    params.put("first", Lists.of("1"));
                    params.put("second", Lists.of("two"));
                    params.put("third", Lists.of("3"));
                }))
                .withQueryParam("second", "2")
                .withQueryParam("fourth", "4")
                .build();

        assertThat(request.getQueryParams()).isEqualTo(Maps.build(params -> {
            params.put("first", Lists.of("1"));
            params.put("second", Lists.of("2"));
            params.put("third", Lists.of("3"));
            params.put("fourth", Lists.of("4"));
        }));
    }

    @Test
    void test_requestParams_canOverride() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example")
                .withRequestParams(Maps.build(params -> {
                    params.put("first", Lists.of("1"));
                    params.put("second", Lists.of("two"));
                    params.put("third", Lists.of("3"));
                }))
                .withRequestParam("second", "2")
                .withRequestParam("fourth", "4")
                .build();

        assertThat(request.getRequestParams()).isEqualTo(Maps.build(params -> {
            params.put("first", Lists.of("1"));
            params.put("second", Lists.of("2"));
            params.put("third", Lists.of("3"));
            params.put("fourth", Lists.of("4"));
        }));
    }

    @Test
    void test_headers_canOverride() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example")
                .withHeaders(Maps.build(headers -> {
                    headers.put("First", Lists.of("1"));
                    headers.put("Second", Lists.of("two"));
                    headers.put("Third", Lists.of("3"));
                }))
                .withHeader("Second", "2")
                .withHeader("Fourth", "4")
                .build();

        assertThat(request.getHeaders()).isEqualTo(Maps.build(headers -> {
            headers.put("First", Lists.of("1"));
            headers.put("Second", Lists.of("2"));
            headers.put("Third", Lists.of("3"));
            headers.put("Fourth", Lists.of("4"));
        }));
    }

    @Test
    void test_headers_canOverride_caseInsensitive() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example")
                .withHeaders(Maps.build(headers -> {
                    headers.put("First", Lists.of("1"));
                    headers.put("first", Lists.of("one"));
                    headers.put("Second", Lists.of("2"));
                }))
                .withHeader("second", "two")
                .build();

        assertThat(request.getHeaders()).isEqualTo(Maps.build(headers -> {
            // FIXME: Kinda inconsistent...
            headers.put("First", Lists.of("1", "one")); // withHeaders - values inside same batch are merged into one list
            headers.put("second", Lists.of("two")); // withHeader - replaces the key and the value
        }));
    }

    @Test
    void test_withDefaultHeaders() {
        TestRequester requester = TestRequester.builder(executor)
                .defaultHeaders(Maps.build(headers -> {
                    headers.put("Authorization", Lists.of("Bearer j.w.t was here"));
                    headers.put("Foo", Lists.of("Bar"));
                }))
                .build();

        RequestData request = requester.get("/example")
                .withHeader("Foo", "Overridden Bar")
                .withHeader("Baz", "Quux")
                .build();

        assertThat(request.getHeaders()).isEqualTo(Maps.build(headers -> {
            headers.put("Authorization", Lists.of("Bearer j.w.t was here"));
            headers.put("Foo", Lists.of("Overridden Bar"));
            headers.put("Baz", Lists.of("Quux"));
        }));
    }

    @Test
    void test_body_bytesArray() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example")
                .withBody("deadbeef".getBytes(StandardCharsets.UTF_8))
                .build();

        // Nothing special here
        assertThat(request.getBody()).isEqualTo("deadbeef".getBytes(StandardCharsets.UTF_8));
    }

    @Test
    void test_body_asString() {
        TestRequester requester = TestRequester.builder(executor).build();

        RequestData request = requester.get("/example")
                .withBody("ëncödë më äs tëxt")
                .build();

        // Encoded as UTF-8 by default (cause why not)
        assertThat(request.getBody()).isEqualTo("ëncödë më äs tëxt".getBytes(StandardCharsets.ISO_8859_1));
    }

    @Test
    void test_body_asForm() {
        TestRequester requester = TestRequester.builder(executor).build();

        Map<String, List<String>> form = Maps.build(params -> {
            params.put("first", Lists.of("1"));
            params.put("second", Lists.of("2"));
        });
        RequestData request = requester.get("/example")
                .withFormBody() // just a shortcut to set the header
                .withRequestParams(form)
                .build();

        assertThat(request.getHeaders()).containsEntry("Content-Type", Lists.of("application/x-www-form-urlencoded"));
        assertThat(request.getRequestParams()).isEqualTo(form);
        assertThat(request.getBody()).isEmpty(); // Let the executor do this job of composing the body or the query string
    }

    @Test
    void test_body_asJson() throws Exception {
        byte[] expectedBody = "{}".getBytes(StandardCharsets.UTF_8);
        given(jsonMapper.toJsonBytes(any())).willReturn(expectedBody);

        TestRequester requester = TestRequester.builder(executor)
                .jsonMapper(jsonMapper)
                .build();
        RequestData request = requester.post("/example")
                .withJsonBody(new Object())
                .build();

        assertThat(request.getHeaders()).containsEntry("Content-Type", Lists.of("application/json;charset=utf-8"));
        assertThat(request.getBody()).isEqualTo(expectedBody);
    }

    @Test
    void test_body_asJson_noJsonMapper() {
        TestRequester requester = TestRequester.builder(executor).build();
        Throwable thrown = catchThrowable(() -> requester.post("/example").withJsonBody(new Object()));

        assertThat(thrown).hasMessageContaining("JsonMapper must be set");
    }

    @Test
    void test_execute_delegatesToExecutor() {
        // ARRANGE
        ResponseData responseData = new ResponseData(
                new Object(),
                204,
                Maps.build(headers -> headers.put("Access-Control-Allowed-Origin", Lists.of("https://example.com"))),
                "rëspönsë".getBytes(StandardCharsets.UTF_8)
        );
        given(executor.execute(any())).willReturn(responseData);

        // ACT
        TestRequester requester = TestRequester.builder(executor).jsonMapper(jsonMapper).build();
        VerifiedResponse response = requester.patch("/example/{id}")
                .withUriVariable("id", "service")
                .withQueryParam("pageNumber", "0")
                .withRequestParam("pageSize", "50")
                .withHeader("Authorization", "j.w.t. was here")
                .withBody("dëädbëëf".getBytes(StandardCharsets.UTF_8))
                .execute();

        // ASSERT
        assertThat(response).isEqualTo(new VerifiedResponse(
                responseData.getRaw(),
                jsonMapper,
                responseData.getStatus(),
                responseData.getBody(),
                responseData.getHeaders()
        ));

        verify(executor).execute(new RequestData(
                "PATCH",
                URI.create("/example/service"),
                Maps.build(queryParams -> {
                    queryParams.put("pageNumber", Lists.of("0"));
                }),
                Maps.build(requestParams -> {
                    requestParams.put("pageSize", Lists.of("50"));
                }),
                Maps.build(headers -> {
                    headers.put("Authorization", Lists.of("j.w.t. was here"));
                }),
                "dëädbëëf".getBytes(StandardCharsets.UTF_8)
        ));
    }

    @Test
    void test_unwrap_withCorrectClass() {
        class InternalRequest {
        }

        InternalRequest raw = new InternalRequest();
        VerifiedResponse response = new VerifiedResponse(raw, null, 200, new byte[0], Maps.build());

        assertThat(response.unwrap(InternalRequest.class)).isSameAs(raw);
    }

    @Test
    void test_unwrap_withWrongClass() {
        class InternalRequest {
        }

        InternalRequest raw = new InternalRequest();
        VerifiedResponse response = new VerifiedResponse(raw, null, 200, new byte[0], Maps.build());

        assertThatThrownBy(() -> response.unwrap(Integer.class))
                .hasMessageContaining("Wrapped response is " + InternalRequest.class);
    }

    @Test
    void test_unwrap_whenRawIsNull() {
        VerifiedResponse response = new VerifiedResponse(null, null, 200, new byte[0], Maps.build());

        assertThatThrownBy(() -> response.unwrap(Integer.class))
                .hasMessageContaining("Wrapped response is null");
    }

    @Test
    void test_getBody_byDefaultIsoEncoding() {
        byte[] expectedBody = "bödy".getBytes(StandardCharsets.ISO_8859_1);

        VerifiedResponse response = new VerifiedResponse(null, null, 200, expectedBody, Maps.build());

        assertThat(response.getBody()).isEqualTo("bödy");
    }

    @Test
    void test_getBody_byExplicitCharset() {
        byte[] expectedBody = "bödy".getBytes(StandardCharsets.UTF_8);

        VerifiedResponse response = new VerifiedResponse(null, null, 200, expectedBody, Maps.build());

        assertThat(response.getBody(StandardCharsets.UTF_8)).isEqualTo("bödy");
    }

    @Test
    void test_getBody_byContentTypeCharset() {
        byte[] expectedBody = "bödy".getBytes(StandardCharsets.UTF_8);

        VerifiedResponse response = new VerifiedResponse(null, null, 200, expectedBody, Maps.build(headers -> {
            headers.put("Content-Type", Lists.of("text/plain;charset=utf-8"));
        }));

        assertThat(response.getBody()).isEqualTo("bödy");
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_assertStatus_valid() {
        return Stream.of(
                arguments(200, (StatusAssertion) StatusAssertions::isOk),
                arguments(201, (StatusAssertion) StatusAssertions::isCreated),
                arguments(204, (StatusAssertion) StatusAssertions::isNoContent),
                arguments(200, (StatusAssertion) StatusAssertions::is2xx),
                arguments(201, (StatusAssertion) StatusAssertions::is2xx),
                arguments(204, (StatusAssertion) StatusAssertions::is2xx),
                arguments(400, (StatusAssertion) StatusAssertions::isBadRequest),
                arguments(401, (StatusAssertion) StatusAssertions::isUnauthorized),
                arguments(403, (StatusAssertion) StatusAssertions::isForbidden),
                arguments(404, (StatusAssertion) StatusAssertions::isNotFound),
                arguments(400, (StatusAssertion) StatusAssertions::is4xx),
                arguments(401, (StatusAssertion) StatusAssertions::is4xx),
                arguments(403, (StatusAssertion) StatusAssertions::is4xx),
                arguments(404, (StatusAssertion) StatusAssertions::is4xx),
                arguments(500, (StatusAssertion) StatusAssertions::isInternalServerError),
                arguments(500, (StatusAssertion) StatusAssertions::is5xx)
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_assertStatus_valid(int status, StatusAssertion assertionMethod) {
        VerifiedResponse response = new VerifiedResponse(null, null, status, new byte[0], Maps.build());

        assertThatCode(() -> assertionMethod.apply(response.assertStatus()))
                .doesNotThrowAnyException();
    }

    interface StatusAssertion extends Function<StatusAssertions, VerifiedResponse> {
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_assertStatus_invalid() {
        return Stream.of(
                arguments(200, (StatusAssertion) StatusAssertions::isBadRequest, "expected: <400> but was: <200>"),
                arguments(201, (StatusAssertion) StatusAssertions::is4xx, "expected: <4XX> but was: <201>"),
                arguments(204, (StatusAssertion) status -> status.is(500), "expected: <500> but was: <204>")
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_assertStatus_invalid(int status, StatusAssertion assertionMethod, String errorMessage) {
        VerifiedResponse response = new VerifiedResponse(null, null, status, new byte[0], Maps.build());

        assertThatCode(() -> assertionMethod.apply(response.assertStatus()))
                .hasMessageContaining(errorMessage);
    }

    @Test
    void test_helpfulAssertionMessage() {
        int status = 400;
        Map<String, List<String>> headers = Maps.build(h -> h.put("Test", Lists.of("Value")));
        String payload = "Boo, invalid params";
        VerifiedResponse response =
                new VerifiedResponse(null, null, status, payload.getBytes(StandardCharsets.ISO_8859_1), headers);

        assertThatThrownBy(() -> response.assertStatus().isOk())
                // The format is important for the IDE to recognize what is actual and what is expected
                .hasMessage("Status in {status: 400, headers: {Test=[Value]}, body: 'Boo, invalid params'} expected: <200> but was: <400>");
    }

    @Test
    void test_getJsonBody_byClass() throws IOException {
        int[] expectedBody = {1, 2, 3};
        given(jsonMapper.fromJson(any(), eq(int[].class))).willReturn(expectedBody);

        VerifiedResponse response = new VerifiedResponse(null, jsonMapper, 200, new byte[0], Maps.build());

        int[] body = response.getJsonBody(int[].class);
        assertThat(body).isSameAs(expectedBody);
    }

    @Test
    void test_getJsonBody_byType() throws Exception {
        List<Integer> expectedBody = Lists.of(1, 2, 3);
        GenericType<List<Integer>> type = new GenericType<List<Integer>>() {
        };
        given(jsonMapper.fromJson(any(), eq(type))).willReturn(expectedBody);

        VerifiedResponse response = new VerifiedResponse(null, jsonMapper, 200, new byte[0], Maps.build());

        Object jsonBody = response.getJsonBody(type);
        assertThat(jsonBody).isSameAs(expectedBody);
    }

    @Test
    void test_getHeaders() {
        Map<String, List<String>> allHeaders = Maps.build(headers -> {
            headers.put("Foo", Lists.of("1", "2", "3"));
            headers.put("Bar", Lists.of("four"));
        });
        VerifiedResponse response = new VerifiedResponse(null, jsonMapper, 200, new byte[0], allHeaders);

        assertThat(response.getHeaders()).isEqualTo(allHeaders);
        assertThat(response.getHeaders("Foo")).isEqualTo(Lists.of("1", "2", "3"));
        assertThat(response.getHeaderFirst("Foo")).contains("1");
        assertThat(response.getHeaders("Bar")).isEqualTo(Lists.of("four"));
        assertThat(response.getHeaderFirst("Bar")).contains("four");
    }

    // TODO: New feature: get test object based on content-type (need to update JsonMapper to take Type, and generic type part of core)
}
