package com.gitlab.testrequester;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.charset.Charset;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ContentTypeTest {

    @SuppressWarnings("unused")
    static Stream<Arguments> test_getCharset() {
        return Stream.of(
                arguments("utf-8", "text/plain; charset=utf-8"),
                arguments("utf-16", "text/plain; charset=\"utf-16\""),
                arguments("utf-16", "form/multipart; charset=\"utf-16\"; boundary=\"---bound---\""),
                arguments(null, "form/multipart"),
                arguments(null, null)
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_getCharset(String expectedCharset, String headerValue) {
        assertThat(ContentType.getCharset(headerValue))
                .isEqualTo(Optional.ofNullable(expectedCharset).map(Charset::forName));
    }
}
