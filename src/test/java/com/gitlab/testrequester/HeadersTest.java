package com.gitlab.testrequester;

import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class HeadersTest {

    @Test
    void test_whenWrapped_keysMerged() {
        // ARRANGE
        Map<String, List<String>> raw = new LinkedHashMap<>();
        raw.put("Multi", Lists.of("111"));
        raw.put("multi", Lists.of("222"));
        raw.put("MULTI", Lists.of("333"));

        // ACT
        Map<String, List<String>> headers = Headers.unwrap(Headers.wrap(raw));

        // ASSERT
        assertThat(headers).hasSize(1).containsEntry("Multi", Lists.of("111", "222", "333"));
    }

    @Test
    void test_whenWrapped_removeRemovesAll() {
        // ARRANGE
        Map<String, List<String>> raw = new LinkedHashMap<>();
        raw.put("Multi", Lists.of("111"));
        raw.put("multi", Lists.of("222"));
        raw.put("MULTI", Lists.of("333"));
        raw.put("Foo", Lists.of("foo1", "foo2"));
        raw.put("Bar", Lists.of("bar"));
        Map<HeaderName, List<String>> headers = Headers.wrap(raw);

        // ACT
        headers.remove(HeaderName.wrap("mUlTi"));

        // ASSERT
        assertThat(Headers.unwrap(headers))
                .hasSize(2)
                .containsEntry("Foo", Lists.of("foo1", "foo2"))
                .containsEntry("Bar", Lists.of("bar"));
    }

    @Test
    void test_whenUnwrapping_oldestNameIsUsed() {
        // ARRANGE
        Map<String, List<String>> raw = new LinkedHashMap<>();
        raw.put("first-SECOND-third", Lists.of("222"));
        raw.put("first-second-THIRD", Lists.of("333"));
        raw.put("FIRST-second-third", Lists.of("111"));

        // ACT
        Map<String, List<String>> unwrapped = Headers.unwrap(Headers.wrap(raw));

        // ASSERT
        assertThat(unwrapped)
                .hasSize(1)
                .containsEntry("first-SECOND-third", Lists.of("222", "333", "111"));
    }
}
