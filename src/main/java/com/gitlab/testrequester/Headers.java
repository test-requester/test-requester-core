package com.gitlab.testrequester;

import lombok.NonNull;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

/**
 * Utility methods to manipulate a map from header names to their values
 */
class Headers {

    private Headers() {
        throw new AssertionError("No instances");
    }

    /**
     * Returns a deep copy of the {@code from} map.
     * <p>
     * Returned map is mutable, while the values of each header name are not.
     */
    static Map<HeaderName, List<String>> copy(@NonNull Map<HeaderName, List<String>> from) {
        Map<HeaderName, List<String>> copy = new LinkedHashMap<>();
        from.forEach((key, values) -> copy.put(key, Lists.copy(values)));
        return copy;
    }

    /**
     * Wraps a map of headers to use case insensitive header names as keys, merging values on collisions.
     * <p>
     * Returned map is mutable, while the values of each header name are not.
     *
     * @see #unwrap(Map)
     */
    static Map<HeaderName, List<String>> wrap(@NonNull Map<String, List<String>> from) {
        return from.entrySet().stream().collect(toMap(
                entry -> HeaderName.wrap(entry.getKey()),
                Map.Entry::getValue,
                Lists::merge,
                LinkedHashMap::new
        ));
    }

    /**
     * Unwraps a map of headers to use plain String header names as keys.
     * <p>
     * Returned map is mutable, while the values of each header name are not.
     * <p>
     * This operation is not symmetrical to wrapping - if the original wrapped map had key collisions, returned map
     * will contain fewer keys.
     *
     * @see #wrap(Map)
     */
    static Map<String, List<String>> unwrap(@NonNull Map<HeaderName, List<String>> from) {
        return from.entrySet().stream().collect(toMap(
                entry -> entry.getKey().toString(),
                Map.Entry::getValue,
                Lists::merge,
                LinkedHashMap::new
        ));
    }
}
