package com.gitlab.testrequester;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * Represents a server response received by {@link TestRequester}.
 */
@Value
@SuppressWarnings("UnusedReturnValue")
public class VerifiedResponse {

    /**
     * Original response as produced by an implementation of {@link RequestExecutor}.
     *
     * @see #unwrap(Class) To retrieve the value.
     */
    @Getter(AccessLevel.NONE)
    Object raw;

    /**
     * A json mapper used to map responses to DTOs.
     */
    @Getter(AccessLevel.PACKAGE)
    @Nullable
    JsonMapper jsonMapper;

    /**
     * Server response status code.
     */
    int status;

    /**
     * Server response body in bytes.
     */
    byte[] bodyBytes;

    /**
     * Server response headers.
     */
    Map<HeaderName, List<String>> headers;

    VerifiedResponse(Object raw, @Nullable JsonMapper jsonMapper, int status, byte[] bodyBytes, Map<String, List<String>> headers) {
        this.raw = raw;
        this.jsonMapper = jsonMapper;
        this.status = status;
        this.bodyBytes = bodyBytes;
        this.headers = Headers.wrap(headers);
    }

    static VerifiedResponse from(ResponseData response, JsonMapper jsonMapper) {
        return new VerifiedResponse(
                response.getRaw(),
                jsonMapper,
                response.getStatus(),
                response.getBody(),
                response.getHeaders()
        );
    }

    /**
     * Returns the wrapped response by casting to a given class.
     * <p>
     * Throws if the given class is wrong, or if no response is wrapped at all.
     */
    public <T> T unwrap(Class<T> tClass) {
        if (!tClass.isInstance(raw)) {
            throw new IllegalArgumentException("Wrapped response is " + (raw != null ? raw.getClass() : null));
        }
        //noinspection unchecked
        return (T) raw;
    }

    public String getBody() {
        return getBody(StandardCharsets.ISO_8859_1);
    }

    public String getBody(Charset defaultCharset) {
        Charset charset = getCharset().orElse(defaultCharset);
        return new String(bodyBytes, charset);
    }

    private Optional<Charset> getCharset() {
        return getHeaderFirst("Content-Type").flatMap(ContentType::getCharset);
    }

    public StatusAssertions assertStatus() {
        return new StatusAssertions(status);
    }

    /**
     * Parses the response body as json into the given DTO type.
     */
    public <T> T getJsonBody(@NonNull Class<T> type) {
        try {
            return requireJsonMapper().fromJson(bodyBytes, type);
        } catch (IOException e) {
            return failedToParse(type.getSimpleName(), e);
        }
    }

    /**
     * Parses the response body as json into the given parameterized DTO type.
     */
    public <T> T getJsonBody(@NonNull GenericType<T> type) {
        try {
            return requireJsonMapper().fromJson(bodyBytes, type);
        } catch (IOException e) {
            return failedToParse(type.getType().getTypeName(), e);
        }
    }

    public Map<String, List<String>> getHeaders() {
        return Headers.unwrap(headers);
    }

    /**
     * Returns server response headers.
     */
    public List<String> getHeaders(@NonNull String name) {
        return headers.getOrDefault(HeaderName.wrap(name), Lists.of());
    }

    /**
     * Returns the first value of a server response header.
     */
    public Optional<String> getHeaderFirst(@NonNull String name) {
        return getHeaders(name).stream().findFirst();
    }

    @Override
    public String toString() {
        StringJoiner result = new StringJoiner(", ", "{", "}");
        result.add("status: " + getStatus());
        result.add("headers: " + getHeaders());
        result.add(bodyBytes.length > 0
                ? "body: '" + getBody() + "'"
                : "body: <not present>");
        return result.toString();
    }

    /**
     * Returns the jsonMapper, checking if it's initialized first.
     */
    private JsonMapper requireJsonMapper() {
        return Objects.requireNonNull(jsonMapper, "JsonMapper not configured");
    }

    private <T> T failedToParse(String typeName, IOException e) {
        throw new AssertionError("Expected body of type " + typeName + " but couldn't parse", e);
    }

    public class StatusAssertions {
        private final int status;

        public StatusAssertions(int status) {
            this.status = status;
        }

        /**
         * Asserts that response status code matches given value.
         */
        public VerifiedResponse is(int expectedStatus) {
            if (expectedStatus != getStatus()) {
                throw new AssertionError(String.format("Status in %s expected: <%s> but was: <%s>",
                        VerifiedResponse.this, expectedStatus, getStatus()));
            }
            return VerifiedResponse.this;
        }

        /**
         * Asserts that response status code is between 200 and 300, end inclusive.
         */
        public VerifiedResponse is2xx() {
            if (status < 200 || status >= 300) {
                throw new AssertionError(String.format("Status in %s expected: <2XX> but was: <%s>",
                        VerifiedResponse.this, getStatus()));
            }
            return VerifiedResponse.this;
        }

        /**
         * Asserts that response status code is between 400 and 500, end inclusive.
         */
        public VerifiedResponse is4xx() {
            if (status < 400 || status >= 500) {
                throw new AssertionError(String.format("Status in %s expected: <4XX> but was: <%s>",
                        VerifiedResponse.this, status));
            }
            return VerifiedResponse.this;
        }

        /**
         * Asserts that response status code is between 500 and 600, end inclusive.
         */
        public VerifiedResponse is5xx() {
            if (status < 500 || status >= 600) {
                throw new AssertionError(String.format("Status in %s expected: <5XX> but was: <%s>",
                        VerifiedResponse.this, status));
            }
            return VerifiedResponse.this;
        }

        /**
         * Asserts that response status code is 200.
         */
        public VerifiedResponse isOk() {
            return is(200);
        }

        /**
         * Asserts that response status code is 201.
         */
        public VerifiedResponse isCreated() {
            return is(201);
        }

        /**
         * Asserts that response status code is 204.
         */
        public VerifiedResponse isNoContent() {
            return is(204);
        }

        /**
         * Asserts that response status code is 400.
         */
        public VerifiedResponse isBadRequest() {
            return is(400);
        }

        /**
         * Asserts that response status code is 401.
         */
        public VerifiedResponse isUnauthorized() {
            return is(401);
        }

        /**
         * Asserts that response status code is 403.
         */
        public VerifiedResponse isForbidden() {
            return is(403);
        }

        /**
         * Asserts that response status code is 404.
         */
        public VerifiedResponse isNotFound() {
            return is(404);
        }

        /**
         * Asserts that response status code is 500.
         */
        public VerifiedResponse isInternalServerError() {
            return is(500);
        }
    }
}
