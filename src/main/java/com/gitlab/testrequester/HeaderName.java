package com.gitlab.testrequester;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Locale;

/**
 * Wraps a {@link String} so that it can be used as a case insensitive key inside a map container - as a header name.
 * <p>
 * The original value can be retrieved using {@link #toString()}.
 */
@AllArgsConstructor(staticName = "wrap")
@EqualsAndHashCode
class HeaderName {
    @EqualsAndHashCode.Exclude
    @NonNull
    private final String string;

    @EqualsAndHashCode.Include(replaces = "string")
    private String cisString() {
        return string.toLowerCase(Locale.ROOT);
    }

    @EqualsAndHashCode.Include(replaces = "string")
    private int cisHashCode() {
        return string.toLowerCase(Locale.ROOT).hashCode();
    }

    @Override
    public String toString() {
        return string;
    }
}
