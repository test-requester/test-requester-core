package com.gitlab.testrequester;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * A utility to perform requests in http controller integration tests with less code.
 * <p>
 * Features include:
 * <ul>
 * <li>Transparent mapping to/from json in request/responses - just set up a JsonMapper first.
 * <li>The URI acts like a template with {@code {placeholderNames}} to be replaced with URI variables.
 * <li>Default headers that are set once on the TestRequester and are inherited by each request.
 * <li>No checked exceptions thrown anywhere - because tests and it's boilerplate.
 * <li>Shortcut methods to check often met response status codes.
 * </ul>
 * <p>
 * Example usages:<br>
 * A short GET call to fetch some resource.
 * <pre>{@code
 * Dto responseBody = requester.get("/some/path").execute().getJsonBody(Dto.class);
 * }</pre>
 * A longer call with different parameters:
 * <pre>{@code
 * Dto responseBody = requester()
 *     .post("/echo/{id}")
 *     .withUriVariable("id", 4567) // replaces the {id} in URI above to become /echo/4567
 *     .withQueryParam("foo", "bar", "{escaped value}") // adds foo=bar&foo=%7Bescaped+value%7D to queryString
 *     .withJsonBody(dto) // Serializes the object to Json and sets Content-Type to application/json
 *     .withHeader("CustomHeader", "some-value") // Adds a header to the request
 *     .execute() // Performs the request returning TestResponse
 *     .assertStatusOk() // Asserts that response status is 200
 *     .getJsonBody(Dto.class) // Parses the response body as json into the given class
 * }</pre>
 */
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class TestRequester {

    private final RequestExecutor requestExecutor;

    @Getter
    @Nullable
    private final JsonMapper jsonMapper;

    @NonNull
    private final Map<HeaderName, List<String>> defaultHeaders;

    public static Builder builder(RequestExecutor executor) {
        return new Builder(executor);
    }

    private TestRequester(@NonNull Builder builder) {
        this.requestExecutor = builder.executor;
        this.jsonMapper = builder.jsonMapper;
        this.defaultHeaders = builder.defaultHeaders;
    }

    public Map<String, List<String>> getDefaultHeaders() {
        return Headers.unwrap(defaultHeaders);
    }

    public TestRequester withDefaultHeaders(@NonNull Map<String, List<String>> defaultHeaders) {
        return new TestRequester(requestExecutor, jsonMapper, Headers.wrap(defaultHeaders));
    }

    /**
     * Returns a request template for a HEAD request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder head(@NonNull String uri) {
        return new RequestBuilder(this, "HEAD", uri);
    }

    /**
     * Returns a request template for a OPTIONS request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder options(@NonNull String uri) {
        return new RequestBuilder(this, "OPTIONS", uri);
    }

    /**
     * Returns a request template for a GET request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder get(@NonNull String uri) {
        return new RequestBuilder(this, "GET", uri);
    }

    /**
     * Returns a request template for a DELETE request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder delete(@NonNull String uri) {
        return new RequestBuilder(this, "DELETE", uri);
    }

    /**
     * Returns a request template for a POST request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder post(@NonNull String uri) {
        return new RequestBuilder(this, "POST", uri);
    }

    /**
     * Returns a request template for a PUT request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder put(@NonNull String uri) {
        return new RequestBuilder(this, "PUT", uri);
    }

    /**
     * Returns a request template for a PATCH request.
     *
     * @param uri See {@link RequestBuilder#getUri()}.
     */
    public RequestBuilder patch(@NonNull String uri) {
        return new RequestBuilder(this, "PATCH", uri);
    }

    @SneakyThrows(Exception.class)
    VerifiedResponse execute(@NonNull RequestBuilder request) {
        ResponseData response = requestExecutor.execute(request.build());
        return VerifiedResponse.from(response, jsonMapper);
    }

    /**
     * Returns the defaultHeaders, but without unwrapping header names.
     * For internal use.
     */
    Map<HeaderName, List<String>> getDefaultCisHeaders() {
        return defaultHeaders;
    }

    @Data
    @Accessors(fluent = true)
    public static class Builder {
        @NonNull
        private final RequestExecutor executor;

        @Nullable
        private JsonMapper jsonMapper;

        @NonNull
        private Map<HeaderName, List<String>> defaultHeaders = Collections.emptyMap();

        public Map<String, List<String>> defaultHeaders() {
            return Headers.unwrap(defaultHeaders);
        }

        public Builder defaultHeaders(@NonNull Map<String, List<String>> defaultHeaders) {
            this.defaultHeaders = Headers.wrap(defaultHeaders);
            return this;
        }

        public TestRequester build() {
            return new TestRequester(this);
        }
    }
}
