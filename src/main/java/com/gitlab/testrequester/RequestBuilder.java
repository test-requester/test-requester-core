package com.gitlab.testrequester;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.ToString;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Represents a request to by made by {@link TestRequester}.
 */
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@EqualsAndHashCode(doNotUseGetters = true)
@ToString(doNotUseGetters = true)
public class RequestBuilder {

    @ToString.Exclude
    private final TestRequester requester;

    /**
     * HTTP method name.
     */
    @Getter
    private final String method;

    /**
     * A URI to execute this request on.
     * <p>
     * Can contain placeholders like {@code "{variablename}"} which will be replaced with values from {@link #uriVariables}.
     */
    @Getter
    private final String uriTemplate;

    /**
     * Variable values to replace placeholders in {@link #uriTemplate}.
     */
    private final Map<String, UriVariable> uriVariables;

    /**
     * Query parameters to execute this request with.
     * <p>
     * These query parameters will go into the queryString of the URI.
     *
     * @see #parameters
     */
    private final Map<String, List<String>> queryParams;

    /**
     * Headers to execute this request with.
     */
    private final Map<HeaderName, List<String>> headers;

    /**
     * Request parameters to execute this request with.
     * <p>
     * These parameters will go into the request body in case of a request with {@code Content-Type: application/x-www-form-urlencoded}.
     *
     * @see #queryParams
     */
    private final Map<String, List<String>> parameters;

    /**
     * Optional body to execute this request with.
     */
    @ToString.Exclude
    private final byte[] body;

    RequestBuilder(@NonNull TestRequester requester, @NonNull String method, @NonNull String uriTemplate) {
        this.requester = requester;
        this.method = method;
        this.uriTemplate = uriTemplate;
        this.body = new byte[0];
        this.headers = requester.getDefaultCisHeaders();
        this.uriVariables = Collections.emptyMap();
        this.queryParams = new LinkedHashMap<>();
        this.parameters = new LinkedHashMap<>();
    }

    /**
     * Returns the {@link #uriTemplate} with all {@link #uriVariables} applied.
     * <p>
     * All URI variables are URL encoded.
     * <p>
     * Any extra uri variables are ignored.
     * If placeholders exist, for which there's no uri variable, an IllegalArgumentException will be thrown.
     * <p>
     * For example, given a uriTemplate "http://example.com/{path}?key={value}"
     *
     * @throws IllegalArgumentException When placeholders exist for which there's no uri variable.
     */
    @SneakyThrows(UnsupportedEncodingException.class)
    public URI getUri() {
        String uri = uriTemplate;
        for (Map.Entry<String, UriVariable> entry : uriVariables.entrySet()) {
            UriVariable variable = entry.getValue();

            String target = "{" + entry.getKey() + "}";
            String replacement = variable.isEncoded()
                    ? URLEncoder.encode("" + variable.getValue(), StandardCharsets.UTF_8.name())
                    : "" + variable.getValue();
            uri = uri.replace(target, replacement);
        }
        return URI.create(uri);
    }

    /**
     * Sets or replaces a single uri variable, from those that will be applied to the URI template.
     * <p>
     * The variable is URI encoded.
     */
    public RequestBuilder withUriVariable(@NonNull String key, @NonNull Object value) {
        return withUriVariable(key, value, /*url encode*/true);
    }

    /**
     * Sets or replaces a single uri variable, from those that will be applied to the URI template.
     */
    public RequestBuilder withUriVariable(@NonNull String key, @NonNull Object value, boolean urlEncode) {
        Map<String, UriVariable> newUriVariables = Maps.build(uriVariables, vars -> {
            vars.put(key, new UriVariable(value, urlEncode));
        });
        return new RequestBuilder(requester, method, uriTemplate, newUriVariables, queryParams, headers, parameters, body);
    }

    /**
     * Returns the query parameters to execute this request with.
     * <p>
     * These query parameters will go into the queryString of the URI.
     */
    public Map<String, List<String>> getQueryParams() {
        return copy(queryParams);
    }

    /**
     * Replaces all query parameters to execute this request with.
     *
     * @see #getQueryParams()
     */
    public RequestBuilder withQueryParams(@NonNull Map<String, List<String>> queryParams) {
        return new RequestBuilder(requester, method, uriTemplate, uriVariables, copy(queryParams), headers, parameters, body);
    }

    /**
     * Sets or replaces a single query parameter to execute this request with.
     *
     * @see #getQueryParams()
     */
    public RequestBuilder withQueryParam(@NonNull String parameterName, @NonNull String... value) {
        Map<String, List<String>> newQueryParams = new LinkedHashMap<>(queryParams);
        newQueryParams.remove(parameterName);
        newQueryParams.put(parameterName, Lists.of(value));
        return withQueryParams(newQueryParams);
    }

    /**
     * Returns the request parameters to execute this request with.
     * <p>
     * These parameters will go into the request body in case of a request with {@code Content-Type: application/x-www-form-urlencoded}.
     */
    public Map<String, List<String>> getRequestParams() {
        return copy(parameters);
    }

    /**
     * Replaces all request parameters to execute this request with.
     *
     * @see #getRequestParams()
     */
    public RequestBuilder withRequestParams(@NonNull Map<String, List<String>> parameters) {
        return new RequestBuilder(requester, method, uriTemplate, uriVariables, queryParams, headers, copy(parameters), body);
    }

    /**
     * Sets or replaces a single request parameter to execute this request with.
     *
     * @see #getRequestParams()
     */
    public RequestBuilder withRequestParam(@NonNull String parameterName, @NonNull String... value) {
        Map<String, List<String>> newParameters = new LinkedHashMap<>(parameters);
        newParameters.remove(parameterName);
        newParameters.put(parameterName, Lists.of(value));
        return withRequestParams(newParameters);
    }

    /**
     * Returns the headers to execute this request with.
     */
    public Map<String, List<String>> getHeaders() {
        return Headers.unwrap(headers);
    }

    /**
     * Replaces all headers to execute this request with.
     *
     * @see #getHeaders()
     */
    public RequestBuilder withHeaders(@NonNull Map<String, List<String>> headers) {
        return new RequestBuilder(requester, method, uriTemplate, uriVariables, queryParams, Headers.wrap(headers), parameters, body);
    }

    /**
     * Sets or replaces a single header to execute this request with.
     *
     * @see #getHeaders()
     */
    public RequestBuilder withHeader(@NonNull String headerName, Collection<String> values) {
        Map<HeaderName, List<String>> newHeaders = Headers.copy(headers);
        newHeaders.remove(HeaderName.wrap(headerName));
        newHeaders.put(HeaderName.wrap(headerName), Lists.copy(values));
        return withCisHeaders(newHeaders);
    }

    /**
     * Sets or replaces a single header to execute this request with.
     *
     * @see #getHeaders()
     */
    public RequestBuilder withHeader(@NonNull String headerName, @NonNull String... value) {
        return withHeader(headerName, Lists.of(value));
    }

    /**
     * Returns the body to execute this request with (empty array if no body).
     */
    public byte[] getBody() {
        return body.clone();
    }

    /**
     * Sets the body to execute this request with.
     *
     * @see #getBody()
     */
    public RequestBuilder withBody(@NonNull byte[] body) {
        return new RequestBuilder(requester, method, uriTemplate, uriVariables, queryParams, headers, parameters, body.clone());
    }

    /**
     * Sets the body to execute this request with.
     *
     * @see #getBody()
     */
    public RequestBuilder withBody(@NonNull String body) {
        // TODO: Gotta use the charset from the content-type header, if any, else default to iso
        return withBody(body.getBytes(StandardCharsets.ISO_8859_1));
    }

    /**
     * Sets the {@code Content-Type} to {@code application/x-www-form-urlencoded}.
     * <p>
     * All parameters returned by {@link #getRequestParams()} will go into the request body.
     */
    public RequestBuilder withFormBody() {
        return withHeader("Content-Type", "application/x-www-form-urlencoded");
    }

    /**
     * Sets the body to execute this request with by serializing given object to json bytes.
     * Also sets the {@code Content-Type} to {@code application/json}.
     */
    @SneakyThrows(IOException.class)
    public RequestBuilder withJsonBody(@NonNull Object body) {
        JsonMapper jsonMapper = requireNonNull(requester.getJsonMapper(), "JsonMapper must be set");
        return withHeader("Content-Type", "application/json;charset=utf-8")
                .withBody(jsonMapper.toJsonBytes(body));
    }

    /**
     * Executes the request.
     */
    public VerifiedResponse execute() {
        return requester.execute(this);
    }

    RequestData build() {
        return new RequestData(getMethod(), getUri(), getQueryParams(), getRequestParams(), getHeaders(), getBody());
    }

    /**
     * Returns a deep copy of the {@code from} map.
     * <p>
     * The returned map is modifiable, but the values are not.
     */
    private Map<String, List<String>> copy(@NonNull Map<String, List<String>> from) {
        Map<String, List<String>> copy = new HashMap<>();
        from.forEach((key, values) -> copy.put(key, Lists.copy(values)));
        return copy;
    }

    /**
     * Similar to {@link #withHeaders(Map)} but takes uses HeaderName as key names.
     */
    private RequestBuilder withCisHeaders(Map<HeaderName, List<String>> headers) {
        return new RequestBuilder(requester, method, uriTemplate, uriVariables, queryParams, Headers.copy(headers), parameters, body);
    }

    /**
     * Print {@link #body} as a String instead of a byte array.<p>
     * In case of any errors, print the base64 encoded version of {@link #body}.
     * <p>
     * This is intended solely for debug purposes.
     */
    @ToString.Include(name = "body")
    @SuppressWarnings("unused"/*Actually used by Lombok*/)
    private String getBodyString() {
        try {
            return new String(body, 0, body.length);
        } catch (Exception e) {
            return "{base64}" + Base64.getEncoder().encodeToString(body);
        }
    }
}
