package com.gitlab.testrequester;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;

class Maps {

    public static <K, V> Map<K, V> buildMut(Consumer<Map<K, V>> builder) {
        Map<K, V> map = new LinkedHashMap<>();
        builder.accept(map);
        return map;
    }

    public static <K, V> Map<K, V> build(Consumer<Map<K, V>> builder) {
        return Collections.unmodifiableMap(buildMut(builder));
    }

    public static <K, V> Map<K, V> build() {
        return Collections.emptyMap();
    }

    public static <K, V> Map<K, V> build(Map<K, V> copy, Consumer<Map<K, V>> builder) {
        Map<K, V> map = new LinkedHashMap<>(copy);
        builder.accept(map);
        return Collections.unmodifiableMap(map);
    }

    private Maps() {
        throw new AssertionError("No instances");
    }
}
