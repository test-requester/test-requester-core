package com.gitlab.testrequester;

import java.nio.charset.Charset;
import java.util.Optional;

class ContentType {

    static Optional<Charset> getCharset(String fullContentType) {
        return getCharsetName(fullContentType).map(Charset::forName);
    }

    private static Optional<String> getCharsetName(String contentType) {
        if (contentType == null) return Optional.empty();

        // For example, given `application/json; charset="utf-8"`
        // Find where `charset=` begins
        String key = "charset=";
        int keyIndex = contentType.indexOf(key);
        if (keyIndex < 0) return Optional.empty();

        // If we found it, the charset name likely starts after `=`.
        int beginIndex = keyIndex + key.length();
        // If the charset name is quoted, must grab everything until the closing quote mark.
        // If not quoted, then until the next directive (next directive starts with `;`)
        char endChar = contentType.charAt(beginIndex) == '"' ? '"' : ';';
        int endIndex = contentType.indexOf(endChar, beginIndex + 1);
        // If there's no end directive, until the end of string
        endIndex = endIndex >= 0 ? endIndex : contentType.length();

        // If quoted, we're only interested about the part between the quote marks
        if (endChar == '"') {
            beginIndex += 1;
        }
        return Optional.of(contentType.substring(beginIndex, endIndex));
    }
}
