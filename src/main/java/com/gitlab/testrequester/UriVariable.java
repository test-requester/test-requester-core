package com.gitlab.testrequester;

import lombok.NonNull;
import lombok.Value;

@Value
class UriVariable {
    @NonNull
    Object value;
    boolean encoded;
}
