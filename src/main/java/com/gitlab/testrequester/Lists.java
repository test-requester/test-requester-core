package com.gitlab.testrequester;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A backport of Java 9 List.of and some more.
 */
class Lists {

    /**
     * Returns an unmodifiable empty list.
     */
    public static <E> List<E> of() {
        return Collections.emptyList();
    }

    /**
     * Returns an unmodifiable list of a single element.
     */
    public static <E> List<E> of(@NonNull E element) {
        return Collections.singletonList(element);
    }

    /**
     * Returns an unmodifiable list of elements.
     * <p>
     * Changing the original array doesn't affect the returned copy.
     */
    @SafeVarargs
    public static <E> List<E> of(@NonNull E... elements) {
        for (int i = 0; i < elements.length; i++) {
            Objects.requireNonNull(elements[i], String.format("%dth element must be non-null", i));
        }
        return copy(Arrays.asList(elements));
    }

    /**
     * Creates an unmodifiable copy of the list.
     * <p>
     * Changing the original list doesn't affect the returned copy.
     */
    public static <E> List<E> copy(@NonNull Collection<E> from) {
        return Collections.unmodifiableList(new ArrayList<>(from));
    }

    /**
     * Merges two lists into a single unmodifiable list.
     */
    public static <E> List<E> merge(@NonNull List<E> left, @NonNull List<E> right) {
        List<E> merged = new ArrayList<>(left);
        merged.addAll(right);
        return Collections.unmodifiableList(merged);
    }

    private Lists() {
        throw new AssertionError("No instances");
    }
}
